firewall-cmd --list-all
firewall-cmd --remove-port 8080/tcp --permanent
firewall-cmd --remove-port 8443/tcp --permanent
firewall-cmd --remove-port 9990/tcp --permanent
firewall-cmd --remove-port 9993/tcp --permanent
firewall-cmd --reload
firewall-cmd --list-all

yes |cp -Rp /etc/selinux/config.bak /etc/selinux/config
setenforce 1

cat /etc/selinux/config
getenforce

systemctl stop jboss
chkconfig jboss off
chkconfig --del jboss
rm -rf /etc/default/jboss-eap.conf
rm -rf /etc/init.d/jboss

rm -rf /usr/share/jboss-eap-7.1
userdel -r jboss
