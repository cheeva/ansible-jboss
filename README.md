# Ansible Role: JBoss

Installs JBoss for RedHat/CentOS linux servers.

## Requirements

JBoss Software or JBoss Patch (Zip Installation)

## Role Variables

Available variables are listed below, along with default values:

JBoss install variables

    # The defaults variables provided by roles/jboss-eap-7.1/defaults/main.yml
    
    # JBoss system user

    jboss_user: jboss
    jboss_uid: 3001
    jboss_group: jboss
    
    # Directory for JBoss Software

    jboss_software_path: /software/jboss-eap-7.1.0.zip

    # Path for install JBoss software

    jboss_install_path: /usr/share/

    # JBoss install path ($JBOSS_HOME)

    jboss_path: /usr/share/jboss-eap-7.1

    # JAVA path ($JAVA_HOME)

    java_path: /usr/lib/jvm/jre-1.8.0-openjdk

    # JBoss management user (JBoss Administration Console)

    jboss_mgmt_user: jboss
    jboss_mgmt_pass: P@ssw0rd

    # JBoss start/stop/restart script

    jboss_initd: /usr/share/jboss-eap-7.1/bin/init.d/jboss-eap-rhel.sh
    jboss_initd_conf: /usr/share/jboss-eap-7.1/bin/init.d/jboss-eap.conf
    jboss_initd_file: /etc/init.d/jboss
    jboss_initd_conf_file: /etc/default/jboss-eap.conf
    jboss_service_name: jboss

Firewalld variables

    # The defaults variables provided by roles/jboss-firewall/defaults/main.yml
    
    # firewall allow port

    firewall_port:
    - 8080/tcp
    - 8443/tcp
    - 9990/tcp
    - 9993/tcp

JBoss patch variables

    # The defaults variables provided by roles/jboss-patch/defaults/main.yml

    # Directory for JBoss patch

    jboss_patch_software_path: /software/jboss-eap-7.1.1-patch.zip

    # JBoss install path ($JBOSS_HOME)

    jboss_path: /usr/share/jboss-eap-7.1
    


## Dependencies

None.

## Install JBoss software

    # ansible-playbook jboss-install.yml

Example Output

    # ansible-playbook jboss-install.yml
    
    PLAY [jboss-eap-7.1] ******************************************************************************************************
    
    TASK [debug] **************************************************************************************************************
    ok: [jboss01.example.com] => {
        "msg": "Beginning install JBoss."
    }
    
    TASK [jboss-firewall : Start and enable firewalld service] ****************************************************************
    ok: [jboss01.example.com]
    
    TASK [jboss-firewall : Set firewall ports config] *************************************************************************
    changed: [jboss01.example.com] => (item=8080/tcp)
    changed: [jboss01.example.com] => (item=8443/tcp)
    changed: [jboss01.example.com] => (item=9990/tcp)
    changed: [jboss01.example.com] => (item=9993/tcp)
    
    TASK [selinux : Set SELINUX from enforcing to permissive] *****************************************************************
    changed: [jboss01.example.com]
    
    TASK [selinux : Edit config file controls the state of SELinux on the system] *********************************************
    ok: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Add group jboss] ************************************************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Add jboss user with a specific uid and a primary group of jboss] ************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Check variables in .bash_profile jboss user] ********************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Add JAVA_HOME to jboss user] ************************************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Add JBOSS_HOME to jboss user] ***********************************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Export JAVA_HOME to jboss user] *********************************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Export JBOSS_HOME to jboss user] ********************************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Set PATH variable to jboss user] ********************************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Check JBoss already installed on the remote machine] ************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Unarchive a JBoss software file that is already on the remote machine] ******************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Change file ownership, group to jboss user] *********************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Check JBoss Management User already added  on the remote machine] ***********************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Add JBoss Management User] **************************************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Copy $JBOSS_HOME/bin/init.d/jboss-eap-rhel.sh to /etc/init.d/jboss] *********************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Copy $JBOSS_HOME/bin/init.d/jboss-eap.conf to /etc/default/jboss-eap.conf] **************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Edit JAVA_HOME in /etc/default/jboss-eap.conf] ******************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Edit JBOSS_HOME in /etc/default/jboss-eap.conf] *****************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Edit JBOSS_USER in /etc/default/jboss-eap.conf] *****************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Edit JBOSS_CONSOLE_LOG in /etc/default/jboss-eap.conf] **********************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Edit JBOSS_OPTS in /etc/default/jboss-eap.conf] *****************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Check JBoss service] ********************************************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Add JBoss service already added  on the remote machine] *********************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Enable JBoss service] *******************************************************************************
    changed: [jboss01.example.com]
    
    TASK [jboss-eap-7.1 : Start JBoss service] ********************************************************************************
    changed: [jboss01.example.com]
    
    RUNNING HANDLER [jboss-firewall : Restart firewalld service] **************************************************************
    changed: [jboss01.example.com]
    
    TASK [debug] **************************************************************************************************************
    ok: [jboss01.example.com] => {
        "msg": "System has been installed JBoss."
    }
    
    PLAY RECAP ****************************************************************************************************************
    jboss01.example.com        : ok=31   changed=27   unreachable=0    failed=0
    
    #

Check JBoss version before install JBoss patch

    # $JBOSS_HOME/bin/standalone.sh -version|grep -i "JBoss"|tail -1
    JBoss EAP 7.1.0.GA (WildFly Core 3.0.10.Final-redhat-1)
    #

Make sure JBoss is started and you should now be able to access the Jboss Console at:

    http://yourdomain.com:8080 

or

    http://yourip:8080

## Install JBoss patch

    # ansible-playbook jboss-patch.yml

Example Output

    # ansible-playbook jboss-patch.yml
    
    PLAY [jboss-patch] *******************************************************************************************************
    
    TASK [debug] *************************************************************************************************************
    ok: [jboss01.example.com] => {
        "msg": "Beginning install patch JBoss."
    }
    
    TASK [jboss-patch : Apply a JBoss EAP patch] *****************************************************************************
    changed: [jboss01.example.com]
    
    RUNNING HANDLER [jboss-patch : Restart JBoss service] ********************************************************************
    changed: [jboss01.example.com]
    
    TASK [debug] *************************************************************************************************************
    ok: [jboss01.example.com] => {
        "msg": "System has been installed patch JBoss."
    }
    
    PLAY RECAP ***************************************************************************************************************
    jboss01.example.com        : ok=4    changed=2    unreachable=0    failed=0
    
    #

Check JBoss version after install JBoss patch

    # $JBOSS_HOME/bin/standalone.sh -version|grep -i "JBoss"|tail -1
    JBoss EAP 7.1.1.GA (WildFly Core 3.0.12.Final-redhat-1)
    #

Make sure JBoss is started and you should now be able to access the Jboss Console at:

    http://yourdomain.com:8080 
    
or

    http://yourip:8080

## License

MIT / BSD

## Author Information

This role was created in 2018 by [Cheeva Rubsrisuksakul].